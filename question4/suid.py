import os
import sys

if len(sys.argv) < 2:
        print("Missing argument\n")
        exit()

print("Hello world\n")

uid = os.getuid()
euid = os.geteuid()

print("uid : %d  eudi : %d\n",uid,euid)

f = open(sys.argv[2], "r")

if f==None :
    print("cannot open file")
    exit()

print("File opens correctly\n")

f.close()