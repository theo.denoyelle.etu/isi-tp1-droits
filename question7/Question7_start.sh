#!/bin/sh
# initialisation des utilisateurs et des groupes puis assignation des groupes au users
sudo adduser --no-create-home --disabled-password admini;

sudo addgroup groupe_a;
sudo addgroup groupe_b;


sudo adduser admini admini;
sudo adduser admini groupe_a;
sudo adduser admini groupe_b;

sudo adduser --no-create-home --disabled-password lambda_a;
sudo adduser lambda_a groupe_a;

sudo adduser --no-create-home --disabled-password lambda_b;
sudo adduser lambda_b groupe_b;


# initialisation des dossiers partagés
mkdir dir_admini;
mkdir dir_a;
mkdir dir_b;

#attribution des droits aux dossiers
sudo chown admini:admini dir_admini;
sudo chown admini:groupe_a dir_a;
sudo chown admini:groupe_b dir_b;
sudo chmod ug+rwx dir_a;
sudo chmod o-rwx dir_a;
sudo chmod +t dir_a;
sudo chmod ug+rwx dir_b;
sudo chmod o-rwx dir_b;
sudo chmod +t dir_b;
sudo chmod ug+rwx dir_admini;
sudo chmod o-rwx dir_admini;
