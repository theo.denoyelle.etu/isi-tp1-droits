########################################
# TESTS SUR lambda_a
su lambda_a "test, lambda_a ecrit dans un fichier du dossier a" > dir_a/fichier_lambda_a.txt
if [ $? -eq 0 ]; then
    echo 'lambda_a reussi a ecrire dans le dossier dir_a:OK'
else
    echo FAIL
fi


su lambda_a touch dir_a/fichier_lambda_a
if [ $? -eq 0 ]; then
    echo 'lambda_a reussi à créer un fichier dans le dossier dir_a:OK'
else
    echo FAIL
fi

su lambda_a touch dir_b/fichier_lambda_a_notOk
if [ $? -eq 0 ]; then
    echo FAIL
else
    echo 'lambda_a ne peut pas creer de fichier dans dir_b:OK'
fi

su lambda_a touch dir_admini/fichier_lambda_a_notOk
if [ $? -eq 0 ]; then
    echo FAIL
else
    echo 'lambda_a ne peut pas creer de fichier dans dir_admini:OK'
fi

su lambda_a ls -l dir_a
if [ $? -eq 0 ]; then
    echo 'lambda_a peut voir le contenu du dossier dir_a:OK'
else
    echo FAIL
fi

su lambda_a ls -l dir_b
if [ $? -eq 0 ]; then
    FAIL
else
    echo 'lambda_a ne peut pas voir le contenu du dossier dir_b:OK'
fi

#########################################
# TESTS SUR lambda_b

su lambda_b "test, lambda_b ecrit dans un fichier du dossier b" > dir_b/fichier_lambda_b.txt
if [ $? -eq 0 ]; then
    echo 'lambda_b reussi a ecrire dans le dossier dir_a:OK'
else
    echo FAIL
fi


su lambda_b touch dir_b/fichier_lambda_b
if [ $? -eq 0 ]; then
    echo 'lambda_b reussi à créer un fichier dans le dossier dir_b:OK'
else
    echo FAIL
fi

su lambda_b touch dir_a/fichier_lambda_b_notOk
if [ $? -eq 0 ]; then
    echo FAIL
else
    echo 'lambda_b ne peut pas creer de fichier dans dir_a:OK'
fi

su lambda_b touch dir_admini/fichier_lambda_b_notOk
if [ $? -eq 0 ]; then
    echo 'lambda_b ne peut pas creer de fichier dans dir_admini:OK'
else
    echo FAIL
fi

su lambda_b ls -l dir_a
if [ $? -eq 0 ]; then
    echo FAIL
else
    echo 'lambda_b ne peut pas voir le contenu du dossier dir_a:OK'
fi



##########################################
# TESTS SUR admini

su admini "test, admini ecrit dans un fichier du dossier a" > dir_a/fichier_admini.txt
if [ $? -eq 0 ]; then
    echo 'admini reussi a ecrire dans le dossier dir_a:OK'
else
    echo FAIL
fi

su admini "test, admini ecrit dans un fichier du dossier b" > dir_b/fichier_admini.txt
if [ $? -eq 0 ]; then
    echo 'admini reussi a ecrire dans le dossier dir_b:OK'
else
    echo FAIL
fi

su admini "test, admini ecrit dans un fichier du dossier admini" > dir_admini/fichier_admini.txt
if [ $? -eq 0 ]; then
    echo 'admini reussi a ecrire dans le dossier dir_admini:OK'
else
    echo FAIL
fi

su admini touch dir_a/fichier_admini
if [ $? -eq 0 ]; then
    echo 'admini reussi à créer un fichier dans le dossier dir_a:OK'
else
    echo FAIL
fi


su admini touch dir_b/fichier_admini
if [ $? -eq 0 ]; then
    echo 'admini reussi à créer un fichier dans le dossier dir_b:OK'
else
    echo FAIL
fi

su admini touch dir_admini/fichier_admini
if [ $? -eq 0 ]; then
    echo 'admini reussi à créer un fichier dans le dossier dir_admini:OK'
else
    echo FAIL
fi

su admini ls -l dir_a
if [ $? -eq 0 ]; then
    echo 'admini peut voir le contenu du dossier dir_a:OK'
else
    echo FAIL
fi

su admini ls -l dir_b
if [ $? -eq 0 ]; then
    echo 'admini peut voir le contenu du dossier dir_b:OK'
else
    echo FAIL
fi


su admini ls -l dir_admini
if [ $? -eq 0 ]; then
    echo 'admini peut voir le contenu du dossier dir_admini:OK'
else
    echo FAIL
fi