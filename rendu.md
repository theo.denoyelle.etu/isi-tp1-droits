# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Denoyelle Theo, theo.denoyelle.etu@univ-lille.fr  

- Nom, Prénom, email: Hadidi Youssef, youssef.hadidi.etu@univ-lille.fr 

## Question 1

l'UID du programme correspondant bien au proprietaire du fichier et les droits de l'utilisateur etant r-- il est impossible au programme d'ecrire sur le fichier même si son groupe lui ( *ubuntu*) possède les droits rw-.

## Question 2

1. Si le x est indiqué sur le repertoire alors on pourra se placer dans le dossier avec cd par exemple.

2. `bash: cd: mydir/: Permission denied` car *toto* ne possedera plus les droits d'exécution du dossier qu'il empruntait au group *ubuntu*. 

3.  
```
ls: cannot access 'mydir/.': Permission denied <br/>
ls: cannot access 'mydir/..': Permission denied <br/>
ls: cannot access 'mydir/truc.txt': Permission denied <br/>
total 0 <br/>
d????????? ? ? ? ?            ? . <br/>
d????????? ? ? ? ?            ? .. <br/>
-????????? ? ? ? ?            ? truc.txt <br/> 
```
Vu qu'il est impossible de se placer dans le dossier, on ne peut voir que les noms mais pas les droits et informations du contenu se trouvant dans le dossier; car *toto* ne possede plus les droits d'exécution du dossier.

## Question 3

1. 

Lorsque l'on est connectés sur la session ubuntu et que le fichier C à été compilé sur la session d'ubuntu et lancé par ubuntu:

```
Hello world
uid : 1000  eudi : 1000
File opens correctly
```
Lorsque le programme est exectué sur la session de toto sans le s flag:

```
Hello world
uid : 1001  eudi : 1001
Cannot open file: Permission denied
```


2. 

Lorsque le programme est exectué sur la session de toto avec le s flag:

```   
Hello world
uid : 1001  eudi : 1000
File opens correctly
```


## Question 4
Lorsque l'on est connectés sur la session ubuntu et que le script est lancé sur la session d'ubuntu:

```   
Hello world

uid :  1000    eudi : 1000
File opens correctly
```  

 Lorsque l'on est connecté sur la session de toto et que l'on lance le script avec le S flag:

```  
Hello world

uid :  1001    eudi : 1001
Traceback (most recent call last):
  File "suid.py", line 17, in <module>
    f = open(sys.argv[1], "r")
PermissionError: [Errno 13] Permission denied: 'mydir/data.txt'
```  

remarque sur les flags : <br/>
*s* : Si le bit setuid ou setgid et le bit exécutable correspondant sont tous deux définis.

*S* : Si le bit setuid ou setgid est défini mais que le bit exécutable correspondant n'est pas défini.
## Question 5
1. 

*chfn* permet de redéfinir des information sur l'utilisateur actuel
<br/>
<br/>
Le fichier chfn appartient a root ainsi qu'au groupe root mais les droits d'execution sont pour tout les utilisateurs, de plus le flag s est attribué a l'utilisateur (ici root) ce qui permet à tous de pouvoir éxécuter ce fichier en ayant des droits équivalents à ceux de root.<br/>
Cependant certaines options ne restent pas utilisables par tous notemment le changement de FullName .
```
-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn
```

2. 
```
chfn -h 03030300033
...
toto:x:1001:1001:,,,03030300033:/home/toto:/bin/bash
```


## Question 6

les mots de passes encryptés sont stockés dans *etc/shadow*

```
 /etc/shadow
           Secure user account information.
```
from *man passwd*

## Question 7
```
drwxrwxr-t 2 admini a_groupe 4096 Jan 20 16:56 dir_a
drwxrwxr-t 2 admini b_groupe 4096 Jan 20 16:35 dir_b
drwxr-xr-t 2 admini admini   4096 Jan 20 16:43 dir_c
=====================================================
a_groupe:x:1002:ajean,afred,admini
b_groupe:x:1003:bjami,admini
ajean:x:1004:
afred:x:1005:
bjami:x:1006:
admini:x:1007:
```

pour lancer les tests de verification on commence par lancer la commande:
```
cd question7
```
puis
```
./Question7_start.sh
```
ceci initialisera les utilisateurs lambda_a lambda_b et admini.
pour lancer les tests on utilise:
```
./Question7_tests.sh
```
## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








